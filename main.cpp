#include <iostream>
#include "array.h"
#include "flagsArray.h"
#include "tinyArray.h"
#include "shiftArray.h"
#include "arrayBalance.h"

const size_t SIZE_OF_ARRAY = 42;
const size_t SIZE_OF_FLAGS_ARRAY = 22;
const size_t SIZE_OF_TINY_ARRAY = 8;
const std::string TASK_SEPARATOR = "========================================================";

/**
 * Print separator between tasks.
 */
void printSeparator()
{
  std::cout << TASK_SEPARATOR << std::endl;
}

int main() {
  // Task 1
  double d_array_to_print[SIZE_OF_ARRAY];
  lesson5::InitArray(d_array_to_print, SIZE_OF_ARRAY);
  lesson5::PrintArray(d_array_to_print, SIZE_OF_ARRAY);
  printSeparator();

  // Task 2
  size_t st_array_of_flags[SIZE_OF_FLAGS_ARRAY];
  lesson5::InitFlagsArray(st_array_of_flags, SIZE_OF_FLAGS_ARRAY);
  lesson5::PrintArray(st_array_of_flags, SIZE_OF_FLAGS_ARRAY);
  lesson5::RevertArrayValues(st_array_of_flags, SIZE_OF_FLAGS_ARRAY);
  lesson5::PrintArray(st_array_of_flags, SIZE_OF_FLAGS_ARRAY);
  printSeparator();

  // Task 3
  size_t st_tiny_array[SIZE_OF_TINY_ARRAY];
  lesson5::InitTinyArray(st_tiny_array, SIZE_OF_TINY_ARRAY);
  lesson5::PrintArray(st_tiny_array, SIZE_OF_TINY_ARRAY);
  printSeparator();

  // Task 4
  int i_array[SIZE_OF_TINY_ARRAY];
  lesson5::InitArray(i_array, SIZE_OF_TINY_ARRAY);
  lesson5::PrintArray(i_array, SIZE_OF_TINY_ARRAY);
  lesson5::ShiftArray(i_array, SIZE_OF_TINY_ARRAY, 2);
  lesson5::PrintArray(i_array, SIZE_OF_TINY_ARRAY);
  lesson5::ShiftArray(i_array, SIZE_OF_TINY_ARRAY, -4);
  lesson5::PrintArray(i_array, SIZE_OF_TINY_ARRAY);
  printSeparator();

  // Task 5
  int i_balance_array[SIZE_OF_TINY_ARRAY];
  lesson5::InitArray(i_balance_array, SIZE_OF_TINY_ARRAY);
  lesson5::PrintBalance(i_balance_array, SIZE_OF_TINY_ARRAY);
  std::cout << (lesson5::CheckBalance(i_balance_array, SIZE_OF_TINY_ARRAY) ? "Array has balance" : "Array hasn't balance") << std::endl;

  // Not so many random arrays has balance so make more tries.
  do {
    lesson5::InitArray(i_balance_array, SIZE_OF_TINY_ARRAY);
  } while(!lesson5::CheckBalance(i_balance_array, SIZE_OF_TINY_ARRAY));
  lesson5::PrintBalance(i_balance_array, SIZE_OF_TINY_ARRAY);
  std::cout << (lesson5::CheckBalance(i_balance_array, SIZE_OF_TINY_ARRAY) ? "Array has balance" : "Array hasn't balance") << std::endl;
  return 0;
}
