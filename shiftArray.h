//
// Created by aard on 17.09.2021.
//

#ifndef LESSON5__SHIFTARRAY_H_
#define LESSON5__SHIFTARRAY_H_

namespace lesson5 {

/**
 * Function to print array.
 *
 * @param arr int
 * Array to print.
 * @param size int
 * Size of array.
 * @return
 * Result of printing true if success.
 */
bool PrintArray(int arr[], size_t size);

/**
 * Function to print array.
 *
 * @param arr double
 * Array to print.
 * @param size int
 * Size of array.
 * @return
 * Result of printing true if success.
 */
bool ShiftArray(int arr[], size_t size, int shift);

/**
 * Set values to array.
 * @param arr int*
 * Array to fill.
 * @param size size_t
 * Size of array.
 * @return
 * Result of filling array true if success.
 */
bool InitArray(int* arr, size_t size);

}

#endif //LESSON5__SHIFTARRAY_H_
