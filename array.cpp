//
// Created by aard on 17.09.2021.
//

#include <iostream>

namespace lesson5 {
/**
 * Task 1
 * Написать функцию которая выводит массив double чисел
 * на экран. Параметры функции это сам массив и его размер.
 * Вызвать эту функцию из main.
 */

bool PrintArray(double arr[], size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < size; i++) {
    std::cout << "Index:" << i << " Value:" << arr[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

bool InitArray(double* arr, size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  for (size_t i = 0; i < size; i++)
  {
    arr[i] = rand() % 100 + (double(rand() % 100) / 100);
  }
  return true;
}

}
