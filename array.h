//
// Created by aard on 17.09.2021.
//

#ifndef LESSON5__ARRAY_H_
#define LESSON5__ARRAY_H_

namespace lesson5 {

/**
 * Function to print array.
 *
 * @param arr double
 * Array to print.
 * @param size int
 * Size of array.
 * @return
 * Result of printing true if success.
 */
bool PrintArray(double arr[], size_t size);

/**
 * Set values to array.
 * @param arr double*
 * Array to fill.
 * @param size size_t
 * Size of array.
 * @return
 * Result of filling array true if success.
 */
bool InitArray(double* arr, size_t size);

}

#endif //LESSON5__ARRAY_H_
