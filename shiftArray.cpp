//
// Created by aard on 17.09.2021.
//

#include <iostream>

namespace lesson5 {
/**
 * Task 4
 * Написать функцию, которой на вход подаётся
 * одномерный массив и число n (может быть положительным,
 * или отрицательным), при этом метод должен циклически
 * сместить все элементы массива на n позиций.
 */

bool PrintArray(int arr[], size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < size; i++) {
    std::cout << "Index:" << i << " Value:" << arr[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

bool ShiftArray(int arr[], size_t size, int shift)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  // If shift negative then we can use right shift with other value for the same result.
  while (shift < 0) {
    shift += size;
  }
  // Skip full cycles.
  while (shift > size) {
    shift -= size;
  }
  for (size_t i = 0; i < shift; i++)
  {
    int last_element = arr[size - 1];
    for (size_t j = size - 1; j > 0; j--) {
      arr[j] = arr[j - 1];
    }
    arr[0] = last_element;
  }
  return true;
}

bool InitArray(int* arr, size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  for (size_t i = 0; i < size; i++)
  {
    arr[i] = rand() % 100;
  }
  return true;
}

}
