//
// Created by aard on 17.09.2021.
//

#include <iostream>

namespace lesson5 {
/**
 * Task 2
 * Задать целочисленный массив, состоящий из элементов 0
 * и 1. Например: [ 1, 1, 0, 0, 1, 0, 1, 1, 0, 0 ]. Написать функцию,
 * заменяющую в принятом массиве 0 на 1, 1 на 0. Выводить
 * на экран массив до изменений и после.
 */

bool PrintArray(size_t arr[], size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  std::cout << "Flags array: " << std::endl;
  for (size_t i = 0; i < size; i++) {
    std::cout << "Index:" << i << " Value:" << arr[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

bool InitFlagsArray(size_t* arr, size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  for (size_t i = 0; i < size; i++)
  {
    arr[i] = rand() % 2;
  }
  return true;
}

bool RevertArrayValues(size_t* arr, size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  for (size_t i = 0; i < size; i++)
  {
    arr[i] ^= 1;
  }
  return true;
}

}
