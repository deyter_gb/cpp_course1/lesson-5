//
// Created by aard on 17.09.2021.
//

#include <iostream>

namespace lesson5 {
/**
 * Task 3
 * Написать функцию которая выводит массив double чисел
 * на экран. Параметры функции это сам массив и его размер.
 * Вызвать эту функцию из main.
 */

const size_t STEP = 3;

bool InitTinyArray(size_t* arr, size_t size)
{
  if (arr == nullptr || size == 0) {
    return false;
  }
  for (size_t i = 0; i < size; i++)
  {
    arr[i] = 1 + i * STEP;
  }
  return true;
}

}
