//
// Created by aard on 18.09.2021.
//

#ifndef LESSON5__ARRAYBALANCE_H_
#define LESSON5__ARRAYBALANCE_H_

namespace lesson5 {

/**
 * Find balance and return value where it located.
 * @param arr int
 * Array to find balance.
 * @param size size_t
 * Size of array.
 * @return
 * Index of balance.
 */
size_t FindBalance(const int arr[], size_t size);

/**
 * Print array and show where balance.
 * @param arr int
 * Array to print.
 * @param size size_t
 * Size of array.
 * @return
 * Result of printing true if success.
 */
bool PrintBalance(int arr[], size_t size);

/**
 * Check if array has balance.
 * @param arr int
 * Array to check.
 * @param size size_t
 * Size of array.
 * @return
 * Result of check true if array has balance.
 */
bool CheckBalance(const int arr[], size_t size);
}

#endif //LESSON5__ARRAYBALANCE_H_
