//
// Created by aard on 17.09.2021.
//

#ifndef LESSON5__TINYARRAY_H_
#define LESSON5__TINYARRAY_H_

namespace lesson5 {

/**
 * Set values to array.
 * @param arr double*
 * Array to fill.
 * @param size size_t
 * Size of array.
 * @return
 * Result of filling array true if success.
 */
bool InitTinyArray(size_t* arr, size_t size);

}

#endif //LESSON5__TINYARRAY_H_
