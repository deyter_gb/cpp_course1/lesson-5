//
// Created by aard on 17.09.2021.
//

#ifndef LESSON5__FLAGS_ARRAY_H_
#define LESSON5__FLAGS_ARRAY_H_

namespace lesson5 {

/**
 * Function to print array.
 *
 * @param arr size_t
 * Array to print.
 * @param size int
 * Size of array.
 * @return
 * Result of printing true if success.
 */
bool PrintArray(size_t arr[], size_t size);

/**
 * Set values to array.
 * @param arr double*
 * Array to fill.
 * @param size size_t
 * Size of array.
 * @return
 * Result of filling array true if success.
 */
bool InitFlagsArray(size_t* arr, size_t size);

/**
 * Change array values from 0 to 1 and from 1 to 0.
 * @param arr size_t
 * Array to revert.
 * @param size size_t
 * Size of array.
 * @return
 * Result of reverting array true if success.
 */
bool RevertArrayValues(size_t* arr, size_t size);

}

#endif //LESSON5__FLAGS_ARRAY_H_
