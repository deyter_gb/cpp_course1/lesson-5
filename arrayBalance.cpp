//
// Created by aard on 17.09.2021.
//

#include <iostream>
#include "shiftArray.h"

namespace lesson5 {
/**
 * Task 5
 * Написать функцию, которой передается не пустой
 * одномерный целочисленный массив, она должна вернуть
 * истину если в массиве есть место, в котором сумма левой и
 * правой части массива равны. Примеры: checkBalance([1, 1, 1, || 2, 1]) → true,
 * checkBalance ([2, 1, 1, 2, 1]) → false,
 * checkBalance ([10, || 1, 2, 3, 4]) → true. Абстрактная граница
 * показана символами ||, эти символы в массив не входят.
 */

size_t FindBalance(const int arr[], size_t size) {
  if (arr == nullptr || size == 0) {
    throw std::invalid_argument("Incorrect array or size");
  }
  int left_sum = 0;
  int right_sum = 0;
  for (size_t i = 0; i < size; i++) {
    right_sum += arr[i];
  }
  for (size_t i = 0; i < size; i++) {
    right_sum -= arr[i];
    left_sum += arr[i];
    if (left_sum == right_sum) {
      return i + 1;
    }
  }

  throw std::invalid_argument("Balance not found");
}

bool PrintBalance(int arr[], size_t size) {
  if (arr == nullptr || size == 0) {
    return false;
  }

  try {
    size_t balance = FindBalance(arr, size);
    std::cout << "Array: " << std::endl;
    for (size_t i = 0; i < size; i++) {
      if (balance == i) {
        std::cout << "Balance here" << std::endl;
      }
      std::cout << "Index:" << i << " Value:" << arr[i] << std::endl;
    }
    std::cout << std::endl;
    return true;
  }
  catch (std::invalid_argument &ex) {
    lesson5::PrintArray(arr, size);
    return false;
  }
}

bool CheckBalance(const int arr[], size_t size){
  try {
    FindBalance(arr, size);
    return true;
  }
  catch (std::invalid_argument &ex) {
    return false;
  }
}
}